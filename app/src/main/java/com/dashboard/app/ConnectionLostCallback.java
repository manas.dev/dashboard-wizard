package com.dashboard.app;

public interface ConnectionLostCallback {
    public void connectionLost();
}
