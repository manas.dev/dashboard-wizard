package com.dashboard.app;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.hardware.usb.UsbDevice;
import android.hardware.usb.UsbManager;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.heapanalytics.android.Heap;
import com.matm.matmsdk.Bluetooth.BluetoothActivity;
import com.matm.matmsdk.MPOS.BluetoothServiceActivity;
import com.matm.matmsdk.MPOS.PosServiceActivity;
import com.matm.matmsdk.Service.JsonAssets;
import com.matm.matmsdk.Utils.EnvData;
import com.matm.matmsdk.Utils.SdkConstants;
import com.matm.matmsdk.aepsmodule.AEPS2HomeActivity;

import java.util.HashMap;

public class MainActivity extends AppCompatActivity {
    RadioGroup rgTransactionType;
    RadioButton rbCashWithdrawal;
    RadioButton rbBalanceEnquiry, rb_mini;
    EditText etAmount, et_paramA, et_paramB;
    Button btnProceed, btn_pair, btn_proceedaeps, upiPagebtn, BtnUnpair, Btndriver, btn_matm1, btnmatm1pair;


    String manufactureFlag = "";
    UsbManager musbManager;
    private UsbDevice usbDevice;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
//        Heap.init(this, "3877737634");

        new JsonAssets(this);

        initView();


        btn_proceedaeps.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                callAEPSSDKApp();
            }
        });

        btn_pair.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, BluetoothServiceActivity.class);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
                        if (checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_DENIED) {
                            requestPermissions(new String[]{
                                    Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.BLUETOOTH,
                                    Manifest.permission.BLUETOOTH_ADMIN, Manifest.permission.BLUETOOTH_PRIVILEGED}, 1001);
                            Toast.makeText(getApplicationContext(), "Please Grant all the permissions", Toast.LENGTH_LONG).show();
                        } else {
                            startActivity(intent);
                        }
                    } else {
                        if (checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_DENIED) {
                            requestPermissions(new String[]
                                    {
                                            Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.BLUETOOTH,
                                            Manifest.permission.BLUETOOTH_ADMIN, Manifest.permission.BLUETOOTH_PRIVILEGED}, 1001);
                            Toast.makeText(getApplicationContext(), "Please Grant all the permissions", Toast.LENGTH_LONG).show();
                        } else {
                            startActivity(intent);
                        }
                    }
                } else {
                    startActivity(intent);
                }
            }
        });

        btnProceed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SdkConstants.transactionAmount = etAmount.getText().toString().trim();
                if (rbCashWithdrawal.isChecked()) {
                    SdkConstants.transactionType = SdkConstants.cashWithdrawal;
                    SdkConstants.transactionAmount = etAmount.getText().toString();
                }
                if (rbBalanceEnquiry.isChecked()) {
                    SdkConstants.transactionType = SdkConstants.balanceEnquiry;
                    SdkConstants.transactionAmount = "0";
                }
                Intent intent = new Intent(MainActivity.this, PosServiceActivity.class);
                startActivity(intent);
            }
        });

    }

    private void initView() {


        musbManager = (UsbManager) getSystemService(Context.USB_SERVICE);


        rgTransactionType = findViewById(R.id.rg_trans_type);
        rbCashWithdrawal = findViewById(R.id.rb_cw);
        rbBalanceEnquiry = findViewById(R.id.rb_be);
        btnProceed = findViewById(R.id.btn_proceed);
        btn_pair = findViewById(R.id.btn_pair);
        rb_mini = findViewById(R.id.rb_mini);
        etAmount = findViewById(R.id.et_amount);
        btn_proceedaeps = findViewById(R.id.proceedBtn);
    }

    private void callAEPSSDKApp() {


        SdkConstants.transactionAmount = etAmount.getText().toString().trim();

        if (rbCashWithdrawal.isChecked()) {

            SdkConstants.transactionType = SdkConstants.cashWithdrawal;

        } else if (rbBalanceEnquiry.isChecked()) {

            SdkConstants.transactionType = SdkConstants.balanceEnquiry;

        } else if (rb_mini.isChecked()) {

            SdkConstants.transactionType = SdkConstants.ministatement;

        }


        SdkConstants.MANUFACTURE_FLAG = manufactureFlag;

        SdkConstants.DRIVER_ACTIVITY = "com.dashboard.app.DriverActivity"; // path to your driver activity


        Intent intent = new Intent(MainActivity.this, AEPS2HomeActivity.class);

        startActivity(intent);

    }

    private boolean biometricDeviceConnect() {
        HashMap<String, UsbDevice> connectedDevices = musbManager.getDeviceList();
        usbDevice = null;
        if (connectedDevices.isEmpty()) {
            deviceConnectMessgae();
            return false;
        } else {
            for (UsbDevice device : connectedDevices.values()) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    if (device != null && device.getManufacturerName() != null) {
                        usbDevice = device;
                        manufactureFlag = usbDevice.getManufacturerName();
                        return true;
                    }
                }
            }
        }

        return false;

    }

    private void deviceConnectMessgae() {
        AlertDialog.Builder builder;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            builder = new AlertDialog.Builder(MainActivity.this,
                    android.R.style.Theme_Material_Light_Dialog_Alert);
        } else {
            builder = new AlertDialog.Builder(MainActivity.this);
        }
        builder.setCancelable(false);
        builder.setTitle("No device found")
                .setMessage("Unable to find biometric device connected . Please connect your biometric device for AEPS transactions.")
                .setPositiveButton(getResources().getString(isumatm.androidsdk.equitas.R.string.ok_error),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        })
                .show();
    }

}